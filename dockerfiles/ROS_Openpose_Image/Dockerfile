# start with the nvidia container for cuda 10 with cudnn7
FROM nvidia/cuda:10.1-cudnn7-devel-ubuntu16.04


LABEL maintainer "Marike Koch <mabr@create.aau.dk>"

# Essentials: developer tools, build tools, OpenBLAS 
RUN apt-get update && apt-get install -y --no-install-recommends \
    apt-utils git curl vim unzip openssh-client wget \
    build-essential cmake \
    libopenblas-dev

# Python 3.5
RUN apt-get install -y --no-install-recommends python3.5 python3.5-dev python3-pip python3-tk && \ 
	pip3 install --no-cache-dir --upgrade pip setuptools # && \
    echo "alias python='python3'" >> /root/.bash_aliases && \
    echo "alias pip='pip3'" >> /root/.bash_aliases

# Science libraries and other common packages 
RUN pip3 --no-cache-dir install \
    numpy scipy sklearn scikit-image pandas matplotlib Cython requests; \
    pip3 install --upgrade pip numpy protobuf

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install wget unzip lsof apt-utils lsb-core -y
RUN apt-get install libatlas-base-dev -y
RUN apt-get install libopencv-dev python-opencv python-pip libgoogle-glog-dev -y   
RUN apt-get install protobuf-compiler -y

# ROS
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

# Setup keys
RUN apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116

# Install ROS
RUN apt-get update
RUN apt-get install -y ros-kinetic-desktop-full

# Initialise rosdep

RUN rosdep init
RUN rosdep update

# Setup ros environment
RUN echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
RUN /bin/bash -c "source ~/.bashrc"
RUN apt-get install -y python-rosinstall python-rosinstall-generator python-wstool build-essential

# Create ROS workspace
RUN mkdir -p home/catkin_ws/src

# Download and unzip the ROS-Openpose wrapper
WORKDIR /home/catkin_ws/src
RUN wget https://github.com/firephinx/openpose_ros/archive/27cf8b40a5ce305b83c86961bda8fb1f5ce46f94.zip; \
unzip 27cf8b40a5ce305b83c86961bda8fb1f5ce46f94.zip; rm 27cf8b40a5ce305b83c86961bda8fb1f5ce46f94.zip
RUN mv openpose_ros-27cf8b40a5ce305b83c86961bda8fb1f5ce46f94 openpose_ros
#Set the path to the openpose models in the openpose-ros wrapper
RUN sed -i 's;/path/to/openpose/models/;/openpose/models/;' openpose_ros/openpose_ros/src/gflags_options.cpp
#Set the default camera topic to be used in the openpose_ros launchfile to be /cv_camera/image_raw
RUN sed -i 's;/camera/image;/cv_camera/image_raw;' openpose_ros/openpose_ros/launch/openpose_ros.launch


# Download and unzip the cv_camera package
RUN wget https://github.com/OTL/cv_camera/archive/db1ea1f0af70681b0eb2c252edb284d115f68aba.zip; \
    unzip  db1ea1f0af70681b0eb2c252edb284d115f68aba.zip; rm  db1ea1f0af70681b0eb2c252edb284d115f68aba.zip
WORKDIR ../../..

# Download and unzip the openpose directory
RUN wget https://github.com/CMU-Perceptual-Computing-Lab/openpose/archive/2268242e260e6282bfe0137a0e3bd8fb7726cea3.zip; \
    unzip 2268242e260e6282bfe0137a0e3bd8fb7726cea3.zip; rm 2268242e260e6282bfe0137a0e3bd8fb7726cea3.zip

RUN mv openpose-2268242e260e6282bfe0137a0e3bd8fb7726cea3 openpose

# Remove the empty placeholder folder caffe and download, unzip and move the caffe repository into a new caffe folder
WORKDIR openpose/3rdparty
RUN rm -rf caffe
RUN wget https://github.com/CMU-Perceptual-Computing-Lab/caffe/archive/b5ede488952e40861e84e51a9f9fd8fe2395cc8a.zip; \
    unzip b5ede488952e40861e84e51a9f9fd8fe2395cc8a.zip; rm b5ede488952e40861e84e51a9f9fd8fe2395cc8a.zip;
RUN mv caffe-b5ede488952e40861e84e51a9f9fd8fe2395cc8a caffe
WORKDIR ..

# Build OpenPose
RUN mkdir -p /openpose/build && cd /openpose/build && cmake -DOpenCV_DIR=/opt/ros/kinetic/share/OpenCV-3.3.1-dev .. 
RUN cd /openpose/build && make -j8 && make install

# Build ROS workspace
RUN /bin/bash -c '. /opt/ros/kinetic/setup.bash; cd /home/catkin_ws; catkin_make'

RUN echo "source /home/catkin_ws/devel/setup.bash" >> ~/.bashrc




